import pandas as pd
from codecarbon import EmissionsTracker
from transformers import pipeline, AutoTokenizer, AutoModelForSequenceClassification


MODEL_NAME = './models/distilbert/'


def accuracy_score(targets, predictions):
    if len(targets) != len(predictions):
        raise ValueError("Input lists must have the same length.")

    correct_predictions = 0
    total_predictions = len(targets)

    for i in range(total_predictions):
        if targets[i] == predictions[i]:
            correct_predictions += 1

    accuracy = correct_predictions / total_predictions
    return accuracy


def convert_label(predicted_label: str) -> int:
    if predicted_label == 'LABEL_0' or predicted_label == 'NEGATIVE':
        return 0
    elif predicted_label == 'LABEL_1' or predicted_label == 'POSITIVE':
        return 1
    else:
        ValueError(f'Unknown label `{predicted_label}`')


def run():
    df = pd.read_csv('./data/test.csv').head(200)
    tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)
    model = AutoModelForSequenceClassification.from_pretrained(MODEL_NAME)
    clf = pipeline('sentiment-analysis', tokenizer=tokenizer, model=model)

    tracker = EmissionsTracker()
    tracker.start()
    predictions = []
    for row in df.itertuples():
        pred = clf(row.text, truncation=True)
        predictions.append(convert_label(pred[0]['label']))
    emissions = tracker.stop()
    targets = df['label'].to_list()

    print('ACCURACY:', round(accuracy_score(targets, predictions), 3))
    print('EMISSIONS:', round(emissions * 1000, 3), 'g CO2eq.')


if __name__ == '__main__':
    run()
