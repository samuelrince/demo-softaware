#!/usr/bin/python3
from os import environ
from datetime import datetime
import os
import time
import requests
from sty import fg, bg, ef
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
from prometheus_api_client import PrometheusConnect, MetricRangeDataFrame


LIFETIME_IN_YEARS = 5
SECONDS_IN_ONE_YEAR = 60 * 60 * 24 * 365
GPU_EMBODIED_GWP_IMPACT = 150

HOST = "192.168.1.64"
BOAGENT_ENDPOINT = f"http://{HOST}:8000"
RUNNER_ENDPOINT = f"http://{HOST}:8085"
PROMETHEUS_ENDPOINT = f"http://{HOST}:9090"
PUSHGATEWAY_ENDPOINT = f"{HOST}:9091"
NOCODB_ENDPOINT = f"http://{HOST}:8081"


def main():
    ci_job_started_at = environ.get("CI_BENCHMARK_STARTED_AT")
    ci_job_stopped_at = environ.get("CI_BENCHMARK_ENDED_AT")
    url = "{}/query?location=FRA&start_time={}&end_time={}".format(
        BOAGENT_ENDPOINT, ci_job_started_at, ci_job_stopped_at
    )
    response = requests.get(url)
    results = response.json()
    results = add_gpu_impacts(results, ci_job_started_at, ci_job_stopped_at)
    push_to_prom(results, ci_job_started_at, ci_job_stopped_at)
    push_to_nocodb(results, ci_job_started_at, ci_job_stopped_at)
    pretty_print_results(results)
    print_summary(results)


def add_gpu_impacts(res, started_at, stopped_at):
    started_at = datetime.fromtimestamp(int(started_at))
    stopped_at = datetime.fromtimestamp(int(stopped_at))
    job_duration_in_seconds = (stopped_at - started_at).total_seconds()

    avg_gpu_power = pull_avg_gpu_power_metrics_from_prom(started_at, stopped_at)
    gpu_energy = avg_gpu_power * job_duration_in_seconds / (3600 * 1000)
    gpu_operational_emissions = gpu_energy * res['emissions_calculation_data']['electricity_carbon_intensity']['value']

    ratio = job_duration_in_seconds / (LIFETIME_IN_YEARS * SECONDS_IN_ONE_YEAR)
    gpu_embodied_emissions = GPU_EMBODIED_GWP_IMPACT * ratio

    res['total_operational_emissions']['value'] += gpu_operational_emissions
    res['embedded_emissions']['value'] += gpu_embodied_emissions
    res['calculated_emissions']['value'] += gpu_operational_emissions + gpu_embodied_emissions
    return res


def pull_avg_gpu_power_metrics_from_prom(started_at, stopped_at) -> float:
    prom = PrometheusConnect(url=PROMETHEUS_ENDPOINT, disable_ssl=True)
    metric = prom.get_metric_aggregation(
        'nvidia_smi_power_draw_watts',
        operations=['average'],
        start_time=started_at,
        end_time=stopped_at,
        step='1'
    )
    avg_power = metric['average']
    return avg_power


def add_gauge_to_registry(name: str, description: str, value: str, registry: CollectorRegistry):
    g = Gauge(name, description, ['ci_job_id', 'project_name', 'ci_job_name'], registry=registry)
    g.labels(environ.get("CI_JOB_ID"), environ.get("CI_PROJECT_NAME"), environ.get("CI_JOB_NAME")).set(value)


def push_to_prom(res, started_at, stopped_at):
    registry = CollectorRegistry()
    add_gauge_to_registry(
        name='softawere_job_operational_GWP',
        description="Global Warming Potential: Operational (run) emissions attributed to this CI job. In {}".format(res["total_operational_emissions"]["unit"]),
        value=res["total_operational_emissions"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_embodied_GWP',
        description="Global Warming Potential: Embodied (manufacture, transport, eol) emissions attributed to this CI job. In {}".format(res["embedded_emissions"]["unit"]),
        value=res["embedded_emissions"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_GWP',
        description="Global Warming Potential: Total (full lifecycle) emissions attributed to this CI job. In {}".format(res["embedded_emissions"]["unit"]),
        value=res["embedded_emissions"]["value"]+res["total_operational_emissions"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_operational_ADPe',
        description="Abiotic Resources Depletion: Operational (run) minerals consumption attributed to this CI job. In {}".format(res["total_operational_abiotic_resources_depletion"]["unit"]),
        value=res["total_operational_abiotic_resources_depletion"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_embodied_ADPe',
        description="Abiotic Resources Depletion: Embodied (manufacture, transport, eol) minerals consumption attributed to this CI job. In {}".format(res["embedded_abiotic_resources_depletion"]["unit"]),
        value=res["embedded_abiotic_resources_depletion"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_ADPe',
        description="Abiotic Resources Depletion: Total (full lifecycle) minerals consumption attributed to this CI job. In {}".format(res["embedded_abiotic_resources_depletion"]["unit"]),
        value=res["embedded_abiotic_resources_depletion"]["value"]+res["total_operational_abiotic_resources_depletion"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_operational_PE',
        description="Primary Energy: Operational (run) primary energy consumption attributed to this CI job. In {}".format(res["total_operational_primary_energy_consumed"]["unit"]),
        value=res["total_operational_primary_energy_consumed"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_embodied_PE',
        description="Primary Energy: Embodied (manufacture, transport, eol) primary energy consumption attributed to this CI job. In {}".format(res["embedded_primary_energy"]["unit"]),
        value=res["embedded_primary_energy"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_PE',
        description="Primary Energy: Total (full lifecycle) primary energy consumption attributed to this CI job. In {}".format(res["total_operational_primary_energy_consumed"]["unit"]),
        value=res["embedded_primary_energy"]["value"]+res["total_operational_primary_energy_consumed"]["value"],
        registry=registry
    )
    add_gauge_to_registry(
        name='softawere_job_total_time',
        description="Time needed by the job. In seconds.",
        value=str(float(stopped_at) - float(started_at)),
        registry=registry
    )

    push_to_gateway(PUSHGATEWAY_ENDPOINT, job="from_ci_jobs", registry=registry)


def get_nocodb_auth_token() -> str:
    signin_url = NOCODB_ENDPOINT + '/api/v1/auth/user/signin'
    response = requests.post(signin_url, json={
        'email': os.environ['NOCODB_USERNAME'],
        'password': os.environ['NOCODB_PASSWORD']
    })
    response.raise_for_status()
    return response.json().get('token')


def push_to_nocodb(res, started_at, stopped_at):
    headers = {'xc-auth': get_nocodb_auth_token()}
    orgs = 'nc'
    project_name = 'Leaderboard'
    table_name = 'Leaderboard submissions'
    insert_url = NOCODB_ENDPOINT + f'/api/v1/db/data/{orgs}/{project_name}/{table_name}'
    response = requests.post(insert_url, headers=headers, json={
        'Team': environ.get("CI_PROJECT_NAME"),
        'Submission date': str(datetime.now()),
        'Job ID': environ.get("CI_JOB_ID"),
        'Job duration': float(stopped_at) - float(started_at),
        'Job embedded impact CO2': res["embedded_emissions"]["value"],
        'Job embedded impact ADP': res["embedded_abiotic_resources_depletion"]["value"],
        'Job embedded impact PE': res["embedded_primary_energy"]["value"],
        'Job operational impact CO2': res["total_operational_emissions"]["value"],
        'Model accuracy': float(os.getenv('ACCURACY_SCORE', '0')),
        'Score': 0.
    })
    if not response.ok:
        print('Error while pushing to nocodb.')

def pretty_print_results(res, level=0):
    indent = ""
    i = 0
    while i < level:
        indent += "\t"
        i+=1
    for k, v in res.items():
        if type(v) is dict:
            print("{}{}{} :".format(indent, fg.red, k))
            pretty_print_results(v, level+1)
        else:
            print("{}{}{} = {}{}".format(indent, fg.white, k, fg.green, v))


def print_summary(res):
    print(ef.bold)
    print(
        "🏭 {}Global Warming Potential = {}Operational Emissions {}+ {}Embdedded Emissions".format(
            fg.white, fg.grey, fg.white, fg.grey
        )
    )
    print(
        "🏭 {}{}{}{} {} = {}{} {}+ {}{}".format(
            bg.yellow,
            fg.white,
            res["calculated_emissions"]["value"],
            bg.rs,
            res["calculated_emissions"]["unit"],
            fg.grey,
            res["total_operational_emissions"]["value"],
            fg.white,
            fg.grey,
            res["embedded_emissions"]["value"]
        )
    )
    print(
        "⛏️ {}Abiotic Resources Depletion = {}Operational Abiotic Resources depletion {}+ {}Embdedded Abiotic Resources depletion".format(
            fg.white, fg.grey, fg.white, fg.grey
        )
    )
    print(
        "⛏️ {}{}{}{} {} = {}{} {}+ {}{}".format(
            bg.yellow,
            fg.white,
            res["total_operational_abiotic_resources_depletion"]["value"]+res["embedded_abiotic_resources_depletion"]["value"],
            bg.rs,
            res["embedded_abiotic_resources_depletion"]["unit"],
            fg.grey,
            res["total_operational_abiotic_resources_depletion"]["value"],
            fg.white,
            fg.grey,
            res["embedded_abiotic_resources_depletion"]["value"]
        )
    )
    print(
        "🛢️ {}Primary Energy Consumption = {}Operational Primary Energy consumption {}+ {}Embdedded Primary Energy Consumption".format(
            fg.white, fg.grey, fg.white, fg.grey
        )
    )
    print(
        "🛢️ {}{}{}{} {} = {}{} {}+ {}{}".format(
            bg.yellow,
            fg.white,
            res["total_operational_primary_energy_consumed"]["value"]+res["embedded_primary_energy"]["value"],
            bg.rs,
            res["embedded_primary_energy"]["unit"],
            fg.grey,
            res["total_operational_primary_energy_consumed"]["value"],
            fg.white,
            fg.grey,
            res["embedded_primary_energy"]["value"]
        )
    )


if __name__ == '__main__':
    main()
